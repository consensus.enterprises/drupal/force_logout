<?php
/**
 * @file
 * Contains \Drupal\force_logout\Controller\ForceLogoutController.
 */
namespace Drupal\force_logout\Controller;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Routing\UrlGeneratorTrait;

class ForceLogoutController {

  use UrlGeneratorTrait;

  /**
   * Provide a confirmation button to trigger complete logout.
   */
  public function confirm() {
    $logout_url = new Url(
      'force_logout.logout',   # route
      [],                      # route parameters
      ['attributes' => [       # options
        'class' => [
          'btn',
          'btn-primary',
          'waves-effect',
          'waves-btn',
          'waves-light',
        ],
      ]]
    );

    $button = Link::fromTextAndUrl(t('Force complete logout'), $logout_url)
      ->toString();

    return array(
      '#type' => 'markup',
      '#markup' => $button,
    );
  }

  /**
   * Forcefully logout of the site.
   */
  public function logout() {

    \Drupal::service('session_manager')->destroy();

    foreach($_COOKIE as $name => $value) {
      setcookie($name, $value, time()-10000, '/');
    }

    drupal_set_message(t('Force logout complete. Please login using the form below.'));

    return $this->redirect('user.login');
  }

}
